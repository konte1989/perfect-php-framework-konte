<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class One2One extends CI_Controller {
    public function index()
    {
        $this->load->view('header');
        $this->load->model('One2');
        $this->load->view('one2one');
        $this->load->view('footer');
    }
}