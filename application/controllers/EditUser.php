<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EditUser extends CI_Controller {
    public function index()
    {
        $this->load->view('header');
        $this->load->view('edituser');
        $this->load->view('footer');
    }
}