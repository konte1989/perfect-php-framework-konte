<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class One2 extends CI_Model
{
    public function newOne2One($recipient, $sender, $message)
    {
        $newOne2one = R::dispense('one2one');
        $newOne2one->recipient = $recipient;
        $newOne2one->sender = $sender;
        $newOne2one->message = $message;
        $newOne2one->readed = 0;
        R::store($newOne2one);
    }

    public function updateReaded($me, $other)
    {
        R::exec('UPDATE one2one SET readed=1 WHERE recipient=:u1id AND sender=:u2id', array(':u1id' => $me, ':u2id' => $other));
    }

    public function getOne2One($me, $other)
    {
        $one2one = R::getAll('SELECT u.firstname, u.lastname, o.message, o.time,
                          CASE WHEN u.profilepic IS NULL THEN "uploads/no-image.jpg"
                          ELSE CONCAT("uploads/", u.profilepic) END AS profilepicture,
                          CASE WHEN u.id=:u1id THEN "even" ELSE "odd" END AS oddeven
                          FROM users u
                          INNER JOIN one2one o ON u.id=o.sender
                          WHERE (o.recipient=:u1id AND o.sender=:u2id) OR (o.recipient=:u2id AND o.sender=:u1id)
                          ORDER BY o.time ASC', array(':u1id' => $me, ':u2id' => $other));

        return $one2one;
    }

    public function allUsersExeptMe($me)
    {
        $allUsersExeptMe = R::getAll('SELECT *, CASE WHEN profilepic IS NULL THEN "uploads/no-image.jpg"
                          ELSE CONCAT("uploads/", profilepic) END AS profilepicture
                          FROM users WHERE id!=:userid', array(':userid' => $me));

        return $allUsersExeptMe;
    }

    public function getAllOne2One($me)
    {
        $AllOne2One = R::getAll('SELECT u.firstname, u.lastname, o.message, o.time, o.id AS messageid,
                          CASE WHEN u.profilepic IS NULL THEN "uploads/no-image.jpg"
                          ELSE CONCAT("uploads/", u.profilepic) END AS profilepicture,
                          CASE WHEN u.id=:u1id THEN "even" ELSE "odd" END AS oddeven
                          FROM users u
                          INNER JOIN one2one o ON u.id=o.sender
                          WHERE o.recipient=:u1id OR o.sender=:u1id
                          ORDER BY o.time DESC
                          LIMIT 10', array(':u1id' => $me));

        usort($AllOne2One, function ($a, $b) {
            return $a['messageid'] - $b['messageid'];
        });

        return $AllOne2One;
    }
}

