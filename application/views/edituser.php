<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$tplvars = array();

$tplvars['authenticated'] = USER_AUTH;

$template = 'edituser';

$tplvars['notification'] = false;

$tplvars['pageTitle'] = 'Perfect PHP framework!';

$tplvars['baseurl'] = base_url();

$submitOK = true;

$errorMessage = '';

if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];

    $currentuser = R::getRow('SELECT *, u.id AS userid FROM users u LEFT JOIN countries c ON u.country=c.countrycode WHERE u.username=:username', array(':username' => $username));
    $tplvars['currentuser'] = $currentuser;

    if (isset($currentuser['mobileprefix']) && !empty($currentuser['mobileprefix'])) {
        $tplvars['prefix'] = $currentuser['mobileprefix'];
    } else {
        $tplvars['prefix'] = '+' . $currentuser['countryprefix'];
    }

    $newprefix = substr($currentuser['mobileprefix'], 1);
    $selectedcountry = R::getRow('SELECT countryname FROM countries WHERE countryprefix=:countryprefix', array('countryprefix' => $newprefix));

    $tplvars['selectedcountry'] = $selectedcountry['countryname'];

    if (isset($currentuser['country']) && !empty($currentuser['country'])) {
        $tplvars['countries'] = R::getAll('SELECT *, CASE WHEN countrycode=:country THEN "selected" ELSE "" END AS selected FROM countries', array('country' => $currentuser['country']));
    } else {
        $tplvars['countries'] = R::getAll('SELECT * FROM countries');
    }

    $tplvars['genderM'] = '';
    $tplvars['genderL'] = '';

    if ($currentuser['gender'] == 'Male') {
        $tplvars['genderM'] = 'checked';
    }

    if ($currentuser['gender'] == 'Female') {
        $tplvars['genderF'] = 'checked';
    }

    $uploads = scandir('uploads');

    if (isset($currentuser['profilepic']) &&
        !empty($currentuser['profilepic']) &&
        !is_null($currentuser['profilepic']) && in_array($currentuser['profilepic'], $uploads)
    ) {
        $tplvars['profilepic'] = 'uploads/' . $currentuser['profilepic'];
    } else {
        $tplvars['profilepic'] = 'uploads/no-image.jpg';
    }
}

if (isset($_POST["submitInfo"])) {
    if (isset($_POST['firstname']) && !empty($_POST['firstname'])) {
        $firstname = $_POST['firstname'];
    } else {
        $submitOK = false;
    }

    if (isset($_POST['lastname']) && !empty($_POST['lastname'])) {
        $lastname = $_POST['lastname'];
    } else {
        $submitOK = false;
    }

    if (isset($_POST['birthday']) && !empty($_POST['birthday'])) {
        $birthday = $_POST['birthday'];
    } else {
        $submitOK = false;
    }

    if (isset($_POST['gender']) && !empty($_POST['gender'])) {
        $gender = $_POST['gender'];
    } else {
        $submitOK = false;
    }

    if (isset($_POST['country']) && !empty($_POST['country'])) {
        $country = $_POST['country'];
    } else {
        $submitOK = false;
    }

    if (isset($_POST['occupation']) && !empty($_POST['occupation'])) {
        $occupation = $_POST['occupation'];
    } else {
        $submitOK = false;
    }

    if (isset($_POST['mobileprefix']) && !empty($_POST['mobileprefix'])) {
        $mobileprefix = $_POST['mobileprefix'];
    } else {
        $submitOK = false;
    }

    if (isset($_POST['mobilenumber']) && !empty($_POST['mobilenumber']) && filter_var($_POST['mobilenumber'], FILTER_VALIDATE_INT)) {
        $mobilenumber = $_POST['mobilenumber'];
    } else {
        $submitOK = false;
        $errorMessage = 'Invalid mobile number!';
    }

    if (isset($_POST['website']) && !empty($_POST['website']) && filter_var($_POST['website'], FILTER_VALIDATE_URL)) {
        $website = $_POST['website'];
    } else {
        $submitOK = false;
        $errorMessage = 'Invalid website!';
    }

    if (isset($_POST['email']) && !empty($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $email = $_POST['email'];
    } else {
        $submitOK = false;
        $errorMessage = 'Invalid email address!';
    }

    if (isset($_POST['about']) && !empty($_POST['about']) && filter_var($_POST['about'], FILTER_SANITIZE_STRING)) {
        $about = $_POST['about'];
    } else {
        $submitOK = false;
        $errorMessage = 'Unallowed characters in About field!';
    }

    if ($submitOK) {
        R::exec('UPDATE users
            SET firstname=:firstname, lastname=:lastname, birthday=:birthday, gender=:gender, country=:country, occupation=:occupation, mobileprefix=:mobileprefix, mobilenumber=:mobilenumber, website=:website, email=:email, about=:about
            WHERE id=:id',
            array('firstname' => $firstname,
                'lastname' => $lastname,
                'birthday' => $birthday,
                'gender' => $gender,
                'country' => $country,
                'occupation' => $occupation,
                'mobileprefix' => $mobileprefix,
                'mobilenumber' => $mobilenumber,
                'website' => $website,
                'email' => $email,
                'about' => $about,
                'id' => $currentuser['userid']));

        $content = '<div class="alert alert-success text-center" role="alert" id="notification">Info update successful...</div>';
        echo $content;
        header("refresh:2;url=/edituser");
    } else {
        $content = '<div class="alert alert-danger text-center" role="alert" id="notification">Error occurred ' . $errorMessage . '</div>';
        echo $content;
    }

}
// Code executed ONLY if submitimg post action is triggered
if (isset($_POST["submitimg"])) {

    if (isset($_FILES["fileToUpload"]["name"])) {
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    }

    // Allow certain file formats
    if ($imageFileType != "jpg") {
        $content = '<div class="alert alert-danger text-center" role="alert" id="notification">Sorry, only JPG files are allowed!</div>';
        echo $content;
        $uploadOk = 0;
    }

    // Check if file already exists
    if (file_exists($target_file)) {
        $filename = pathinfo($target_file, PATHINFO_FILENAME);
        if (isset($_FILES['fileToUpload']['tmp_name'])) {
            // Directory related to the location of your gyazo script
            $fileCount = count(glob('uploads/*.jpg'));
            $newName = 'uploads/' . $filename . ($fileCount + 1) . '.jpg';
        }
    } else {
        $newName = $target_file;
    }

    $size = 2 * 1024 * 1024;
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > $size) {
        $content = '<div class="alert alert-danger text-center" role="alert" id="notification">Sorry, your file is too large!</div>';
        echo $content;
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 1) {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $newName)) {
            $content = '<div class="alert alert-success text-center" role="alert" id="notification">Image upload successful!</div>';
            echo $content;
            R::exec('UPDATE users SET profilepic=:profilepic WHERE id=:id', array('profilepic' => $_FILES["fileToUpload"]["name"], 'id' => $currentuser['userid']));
            header("refresh:2;url=/edituser");
        }
    }
}

if (isset($_POST["submitPassword"])) {

    $salt = 'secret_key_on_left';
    $pepa = 'secret_key_on_right';
    $hash = md5($salt . $password . $pepa);

}

echo T::mustache($template, $tplvars);