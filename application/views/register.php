<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$html = array();

$template = 'register';

$tplvars = array();

if(isset($_POST['submit'])) {
    if(isset($_POST['firstname']) && !empty($_POST['firstname']) && isset($_POST['lastname']) && !empty($_POST['lastname']) && isset($_POST['usernamee']) && !empty($_POST['usernamee']) && isset($_POST['passworde']) && !empty($_POST['passworde'])) {
        $username = $_POST['usernamee'];
        $password = $_POST['passworde'];
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];

        $salt = 'secret_key_on_left';
        $pepa = 'secret_key_on_right';
        $hash = md5($salt . $password . $pepa);

        $exist = R::getAll('SELECT * FROM users WHERE username=:username', array(':username' => $username));

        if (isset($exist) && !empty($exist)) {
            echo "User already exist!";
            header( "refresh:2;url=/register" );
            exit;
        } else {
            $user = R::dispense('users');
            $user->firstname = $firstname;
            $user->lastname = $lastname;
            $user->username = $username;
            $user->password = $hash;
            R::store($user);
            $tplvars['success'] = true;
            echo T::mustache($template, $tplvars);
            header( "refresh:5;url=/" );
            exit;
        }
    }
}

echo T::mustache($template, $tplvars);