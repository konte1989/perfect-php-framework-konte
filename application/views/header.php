<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'views/login.php');

$tplvars = array();

$tplvars['authenticated'] = USER_AUTH;

$template = 'header';

$tplvars['pageTitle'] = 'Perfect PHP framework!';

if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];



    $currentuser = R::getRow('SELECT * FROM users WHERE username=:username', array(':username' => $username));

    $one2one = R::getAll('SELECT *, CASE WHEN u.profilepic IS NULL THEN "uploads/no-image.jpg" ELSE CONCAT("uploads/", u.profilepic) END AS profilepicture FROM one2one o JOIN users u ON o.sender=u.id WHERE recipient=:id AND readed = 0', array('id' => $currentuser['id']));

    $tplvars['countOne2One'] = count($one2one);

    $tplvars['messagesOne2One'] = $one2one;

    $uploads = scandir('uploads');

    $tplvars['fullname'] = $currentuser['firstname'] . ' ' . $currentuser['lastname'];
    if (isset($currentuser['profilepic']) &&
        !empty($currentuser['profilepic']) &&
        !is_null($currentuser['profilepic']) && in_array($currentuser['profilepic'], $uploads)
    ) {
        $tplvars['profilepic'] = 'uploads/' . $currentuser['profilepic'];
    } else {
        $tplvars['profilepic'] = 'uploads/no-image.jpg';
    }
    $tplvars['lastlogin'] = date('H:i', strtotime($currentuser['lastlogin']));
}

$tplvars['baseurl'] = base_url();

echo T::mustache($template, $tplvars);