<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$html = array();

$template = 'one2one';

$tplvars = array();

$tplvars['authenticated'] = USER_AUTH;

$newOne2One = new One2();

if (isset($_SESSION['username'])) {

    $username = $_SESSION['username'];

    $currentuser = R::getRow('SELECT * FROM users WHERE username=:username', array(':username' => $username));

    $tplvars['currentuser'] = $currentuser;

    $tplvars['allUsersExeptMe'] = $newOne2One->allUsersExeptMe($currentuser['id']);

    if (isset($_GET['user']) && !empty($_GET['user'])) {

        $tplvars['perUser'] = true;

        $userid = $_GET['user'];

        $tplvars['userid'] = $userid;

        if (isset($_POST['one2onesubmit'])) {
            if (isset($_POST['newOne2OneMessage']) && !empty($_POST['newOne2OneMessage'])) {

                $newOne2One->newOne2One($userid, $currentuser['id'], $_POST['newOne2OneMessage']);
            }
        }

        $otheruser = R::getRow('SELECT * FROM users WHERE id=:userid', array(':userid' => $userid));

        $tplvars['otheruser'] = $otheruser;

        $tplvars['one2oneMessages'] = $newOne2One->getOne2One($currentuser['id'], $userid);

        $newOne2One->updateReaded($currentuser['id'], $userid);
    } else {
        $tplvars['allUsers'] = true;

        $tplvars['allOne2One'] = $newOne2One->getAllOne2One($currentuser['id']);
    }
}

echo T::mustache($template, $tplvars);